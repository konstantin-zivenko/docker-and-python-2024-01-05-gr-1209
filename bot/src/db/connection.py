from psycopg import connect, OperationalError

from bot.src.settings import settings


def create_connection(
        db_name=settings.DB_NAME,
        db_user=settings.DB_USER,
        db_password=settings.DB_PASSWORD,
        db_host=settings.DB_HOST,
        db_port=settings.DB_PORT,
):
    try:
        print("try connection ...")
        connection = connect(
            dbname=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
        )
        print("Connection to PostgreSQL DB successful")
    except OperationalError as err:
        print(f"The error '{err}' occurred")
        connection = None
    return connection

conn = create_connection()

